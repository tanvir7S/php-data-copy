<?php

function getNewConnection() {
    $conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['database'], $GLOBALS['port']);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}

function disconnect($conn){
    mysqli_close($conn);
}

function getRefreshedConnection($conn){
    disconnect($conn);
    sleep(2);

    return getNewConnection();
}

function runQuery($conn, $sql){
    if ($conn->query($sql) !== TRUE) {
        echo "Error: " . $sql . "- " . $conn->error . "\n";
        return false;
    }

    return true;
}

function getQueryResult($conn, $sql){
    $result = $conn->query($sql);
    if ($result->num_rows <= 0) {
        echo "0 results" . "\n";
        return null;
    }

    return $result->fetch_all(MYSQLI_ASSOC);
}
