<?php

require __DIR__ . '/functions.php';

$val = getopt("h:u:p:d:z:");

$host = $val['h'];
$user = $val['u'];
$pass = $val['p'];
$database = $val['d'];
$port =  $val['z'];

if (($host==null) || ($user==null) || ($pass==null) || ($database==null) || ($port==null)) {
    die('Missing Required Param (h, u, p, d or z)');
}

$tablesToMigrate = [
    'notification_log',
    'ticket_status_history',
    'ticket'
];
$parentTable = 'ticket';
$childTables = [
    'notification_log',
    'ticket_status_history'
];
$parentColumn = 'ticket_id';
$backupTableSuffix = '_archive';
$parentTableWhereColumn = 'record_updated_at';

$chunkSize = 50000;
$dateLowerLimit = (new DateTime('first day of this month - 6 months'))->format('Y-m-d');

$conn = getNewConnection();

foreach($tablesToMigrate as $table){
    //create an empty table with the column structure of basetable
    $sql = "CREATE TABLE IF NOT EXISTS ". $table . $backupTableSuffix . " LIKE " .$table .";";
    if (!runQuery($conn, $sql)) {
        die("Could not create backup table - ". $table . $backupTableSuffix);
    }
}

//get id of last record in parent table that we want to keep
$sql = "select id from ".$parentTable." where ".$parentTableWhereColumn." >= '".$dateLowerLimit."' order by ".$parentTableWhereColumn." asc limit 1;";
$result = getQueryResult($conn, $sql);
if(!$result){
    die("No records found in ".$parentTable." table");
}

$parentTableIdToStart = $result[0]['id']-1;
// check if there are entries in parent table starting from $parentTableIdToStart
$sql = "select id from ".$parentTable." where id <= '".$parentTableIdToStart."' limit 1;";
$result = getQueryResult($conn, $sql);
if(!$result){
    die("No records found in ".$parentTable." table from ".$parentTableIdToStart." to copy from");
}

echo "Starting with ".$parentTable." table ID: ".$parentTableIdToStart."\n\n";;

while ($parentTableIdToStart > 0){
    $lowerLimit = $parentTableIdToStart - $chunkSize;
    if($lowerLimit <= 0){
        $lowerLimit = 1;
    }

    echo "I copy rows from ".$parentTableIdToStart." to ".$lowerLimit."\n";

    //copy child table entries to their respective backup tables
    foreach ($childTables as $childTable){
        $sql = "INSERT INTO ".$childTable.$backupTableSuffix." SELECT * FROM ".$childTable." WHERE ".$parentColumn." between ".$lowerLimit." AND ".$parentTableIdToStart.";";
        if (!runQuery($conn, $sql)) {
            continue;
        }
        echo "Successful Rows copied for $childTable with $parentColumn from $parentTableIdToStart to $lowerLimit."."\n";
    }

    //copy parent table entries to its respective backup table
    $sql = "INSERT INTO ".$parentTable.$backupTableSuffix." SELECT * FROM ".$parentTable." WHERE id between ".$lowerLimit." AND ".$parentTableIdToStart.";";
    if (!runQuery($conn, $sql)) {
        continue;
    }
    echo "Successful Rows copied for $parentTable with id from $parentTableIdToStart to $lowerLimit"."\n";

    //delete parent table entries - would automatically delete child table entries
    $sql = "DELETE FROM ".$parentTable." WHERE id between ".$lowerLimit." AND ".$parentTableIdToStart.";";
    if (!runQuery($conn, $sql)) {
        continue;
    }
    echo "Successful Rows deleted for $parentTable with id from $parentTableIdToStart to $lowerLimit"."\n\n";

    $conn = getRefreshedConnection($conn);
    $parentTableIdToStart = $lowerLimit-1;
}

disconnect($conn);
