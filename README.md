# php-data-copy
Using this script, we can archive ticket table entries(older than 6 months) and its respective child table entries

# Usage
php main.php -h <host> -u <user> -p <password> -d <database> -z <port>